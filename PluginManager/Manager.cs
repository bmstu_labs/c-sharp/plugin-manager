﻿using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Parsing;
using System.Reflection;

namespace PluginManager;
internal class Manager
{
    private List<IPlugin> Plugins { get; } = new();

    private void LoadPlugins()
    {
        string[] dlls = Directory.GetFiles(Path.GetDirectoryName(RootCommand.ExecutablePath) +  "/plugins", "*.dll");

        foreach (var dll in dlls)
        {
            var pluginInfo = Assembly.LoadFrom(dll);
            var types = pluginInfo.DefinedTypes;
                
            Type? type = types
                .Where(t => t.GetInterfaces().Contains(typeof(IPlugin)))
                .FirstOrDefault();
            if (type != null)
            {
                ConstructorInfo? constructorInfo = type
                    .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                    .Where(ctor => ctor.GetParameters().Length == 0)
                    .FirstOrDefault();

                if (constructorInfo != null)
                    Plugins.Add((IPlugin)constructorInfo.Invoke(null));
            }   
        }
    }

    public Manager()
    {
        LoadPlugins();
    }

    public List<string> GetMenu()
    {
        List<string> result = new();

        foreach (var plugin in Plugins)
            result.Add(plugin.Command.Name + ": " + plugin.Command.Description);

        return result;
    }

    public List<Command> GetCommands()
    {
        List<Command> result = new();

        foreach (var plugin in Plugins)
            result.Add(plugin.Command);

        return result;
    }

    public void ExecutePlugin(int index, string[] args)
    {
        Plugins[index].Command.Invoke(args);
        /*var obj = Plugins[index].Command.Invoke(args);// Run(args);
        if (obj != null)
            Console.WriteLine(obj.ToString());*/
    }

    public async Task Handle(params string[] args)
    {
        var root = new RootCommand("Plugin manager");

        foreach (var command in GetCommands())
            root.Add(command);

        var parser = new CommandLineBuilder(root)
            .UseDefaults()
            .UseHelp()
            .Build();

        await parser.InvokeAsync(args);
    }

}
