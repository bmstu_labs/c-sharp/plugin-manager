﻿using System;
using System.CommandLine.Invocation;
using System.Reflection;

namespace PluginManager;

internal class Program
{
    static async Task Main(string[] args)
    {
        var manager = new Manager();
        await manager.Handle(args);
    }
}