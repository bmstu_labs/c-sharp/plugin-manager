Консольное приложение C#, позволяющее подключать плагины

Плагин должен иметь класс, реализовующий интерфейс из IPlugin из PluginManager.SDK

- PluginManager - приложение
- PluginManager.SDK - библиотека с контрактным интерфейсом, должна подключаться к любому плагину
- Plugins - папка с парой реализованных плагинов:
    - "HelloWorld" - выводит надпить "Hello, World!" в консоль
    - "rpn" - обратная польская запись. Умеет преобразовывать инфксную форму в обратную польскую нотацию и вычислять введеное выражение
    
Построено с использование [System.CommandLine](https://learn.microsoft.com/ru-ru/dotnet/standard/commandline/get-started-tutorial)
    
Скриншот работы (Windows)
![Скриншот работы (Windows)](windows.png "Скриншот работы (Windows)")
Скриншот работы (Linux)
![Скриншот работы (Linux)](linux.png "Скриншот работы (Linux)")

