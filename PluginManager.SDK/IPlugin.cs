﻿using System.CommandLine;

namespace PluginManager;
public interface IPlugin
{
    public Command Command { get; }
}