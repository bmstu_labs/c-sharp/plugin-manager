﻿using PluginManager;
using System.CommandLine;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace ReversePolishNotation;

[Flags]
public enum rpnErrors
{
    rpneNone = 0,
    rpneBrackets,
    rpneVariables,
    rpneExpression,
    //rpneDomain,
    rpneOperator,
}

public static class NotationElement
{
    public static string[] Operators = new string[] { "+", "-", "*", "/", "^", "sin", "cos", "abs" };
    public static sbyte GetPriority(this string element)
    {
        switch (element)
        {
            case "+":
            case "-": return 1;

            case "*":
            case "/": return 2;

            case "^": return 3;

            case "sin":
            case "cos":
            case "abs": return 4;

            default: return -1;
        }
    }

    public static bool GetDouble(this string element, out double value)
    {
        return double.TryParse(element, out value);
    }

    public static bool IsDouble(this string element)
    {
        return element.GetDouble(out double val);
    }

    public static bool IsVar(this string element)
    {
        return
            !element.IsDouble()
            && !element.IsOperator()
            && !(new string[] { "(", ")", " " }).Contains(element);
    }

    public static bool IsOperator(this string element)
    {
        return Operators.Contains(element);
    }

    public static bool IsBinaryOperator(this string element)
    {
        switch (element)
        {
            case "+":
            case "-":
            case "*":
            case "/": return true;

            default: return false;
        }
    }
}

delegate decimal BinaryOperation(decimal a, decimal b);
delegate decimal UnaryOperation(decimal a);

public class RPN: IPlugin
{
    List<string> rpn;
    public List<string> vars;
    rpnErrors _error;

    public string ErrorMessage
    {
        get
        {
            return _error switch
            {
                rpnErrors.rpneBrackets => "Wrong brackets",
                rpnErrors.rpneVariables => "Not all variables bound",
                //rpnErrors.rpneDomain => "Wrong domain of function",
                rpnErrors.rpneOperator => "Wrong operator",
                rpnErrors.rpneExpression => "Wrong expresion",
                _ => ""
            };
        }
    }

    public Command Command
    {
        get
        {
            var command = new Command("rpn", "Обратная польская нотация");

            var expression = new Argument<string>("expression", "Арифметическое выражение в инфиксной форме")
            {
                Arity = ArgumentArity.ExactlyOne
            };

            command.Add(expression);

            var display = new Command("display", "Вывод выражения в форме обратной польской записи");

            display.SetHandler((e) =>
            {
                var rpn = RPN.FromInfixNotation(e);
                Console.WriteLine(rpn.ToString());
            }, expression);

            command.Add(display);

            var eval = new Command("eval", "Вычисление введенного выражения");

            var variables = new Option<double[]>(
                aliases: new[] { "-v", "--vars" },
                description: "Переменные")
            {
                IsRequired = false,
                Arity = ArgumentArity.OneOrMore
            };

            eval.Add(variables);

            eval.SetHandler((e, v) =>
            {
                var rpn = RPN.FromInfixNotation(e);
                var result = rpn.Evaluate(v);
                Console.WriteLine(rpn._error != 0 ? rpn._error.ToString() : result);
            }, expression, variables);

            command.Add(eval);

            return command;
        }
    }

    private RPN()
    {
        rpn = new List<string>();
        vars = new List<string>();
    }

    public RPN(string rpnNotation) : this()
    {
        if (rpnNotation == "")
            return;

        rpn.AddRange(rpnNotation.Split(" "));

        vars.AddRange((from op in rpn where op.IsVar() select op).Distinct());
    }

    public RPN(rpnErrors error) : this("")
    {
        _error = error;
    }

    private static List<string> SplitString(string s)
    {
        List<string> list = new();
        var sb = new StringBuilder(s);

        foreach (var op in NotationElement.Operators.Union(new string[] { "(", ")" }))
            sb.Replace(op, " " + op + " ");

        list.AddRange(new Regex(@"^ -")
            .Replace(new Regex(@"\s+").Replace(sb.ToString(), " "), "0 -")
            .Replace("( -", "( 0 -")
            .Trim()
            .Split(" "));

        return list;
    }

    public static RPN FromInfixNotation(string InfixNotation)
    {
        var stack = new Stack<string>();
        string output = "";

        var elements = SplitString(InfixNotation);

        for (int i = 0; i < elements.Count;)
        {
            var s = elements[i];
            if (s == "(")
                stack.Push(s);
            else if (s == ")")
            {
                string? peak;
                while (stack.TryPeek(out peak) && peak != "(")
                    output += " " + stack.Pop();

                if (peak == null)
                    return new RPN(rpnErrors.rpneBrackets);

                stack.Pop();
            }
            else if (s.IsDouble() || s.IsVar())
                output += " " + s;
            else if (stack.Count == 0 || stack.Peek() == "(")
                stack.Push(s);
            else
            {
                if (stack.Peek().GetPriority() < s.GetPriority())
                    stack.Push(s);
                else
                {
                    output += " " + stack.Pop();
                    continue;
                }
            }
            i++;
        }

        while (stack.Count > 0)
        {
            if (stack.Peek() == "(")
                return new RPN(rpnErrors.rpneBrackets);

            output += " " + stack.Pop();
        }

        return new RPN(output.Trim());
    }

    public override string ToString()
    {
        if (_error != rpnErrors.rpneNone)
            return ErrorMessage;

        return rpn.Count == 0 ? "" : string.Join(" ", rpn);
    }

    public double? Evaluate(params double[]? variables)
    {
        if (_error != 0 || rpn.Count == 0)
            return null;

        var stack = new Stack<double>();

        foreach (string op in rpn)
        {
            if (!op.IsOperator())
            {
                if (op.IsVar())
                {
                    if ((variables?.Length ?? 0) < vars.Count)
                    {
                        _error = rpnErrors.rpneVariables;
                        return null;
                    }
                    else
                        if (vars.IndexOf(op) == -1)
                        {
                            _error = rpnErrors.rpneVariables;
                            return null;
                        }
                        else
                            stack.Push(variables![vars.IndexOf(op)]);
                }
                else
                    stack.Push(double.Parse(op.Replace('.', ',')));
            }
            else
            {
                double b;

                switch (op)
                {
                    case "+": stack.Push(stack.Pop() + stack.Pop()); break;
                    case "-": b = stack.Pop(); stack.Push(stack.Pop() - b); break;
                    case "*": stack.Push(stack.Pop() * stack.Pop()); break;
                    case "/":
                        {
                            b = stack.Pop();
                            if (b == 0)
                                return null;
                            stack.Push(stack.Pop() / b);
                            break;
                        }
                    case "^": b = stack.Pop(); stack.Push(Math.Pow(stack.Pop(), b)); break;
                    case "sin": stack.Push(Math.Sin(stack.Pop())); break;
                    case "cos": stack.Push(Math.Cos(stack.Pop())); break;
                    case "abs": stack.Push(Math.Abs(stack.Pop())); break;
                    default:
                        {
                            _error = rpnErrors.rpneOperator;//throw new ArithmeticException("Ошибка выражения");
                            return null;
                        }
                }
            }
        }
        if (stack.Count == 1)
            return stack.Pop();
        else
        {
            _error = rpnErrors.rpneExpression;//throw new ArithmeticException("Ошибка выражения");
            return null;
        }
    }
}
