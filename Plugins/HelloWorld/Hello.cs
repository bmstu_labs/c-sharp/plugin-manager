﻿using PluginManager;
using System.CommandLine;

namespace HelloWorld;

public class Hello: IPlugin
{
    public Command Command
    {
        get
        {
            var command = new Command("hello", "Выводит надпись \"Hello, World!\"");

            command.SetHandler(() =>
            {
                Console.WriteLine("Hello, World!");
            });

            return command;
        }
    }
}
